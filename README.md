# PSO-Clustering
Apache Spark 2.3.1

## References
[1]M. Sherar and F. Zulkernine, “Particle swarm optimization for large-scale clustering on apache spark,” in 2017 IEEE Symposium Series on Computational Intelligence (SSCI), 2017, pp. 1–8.

[2]D. V. D. Merwe and A. Engelbrecht, “Data clustering using particle swarm optimization,” in In IEEE congress on evolutionary computation, 2003, pp. 215–220.
q
