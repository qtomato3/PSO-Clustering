import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Util {
    public static Double[] zeroes(int size) {
        Double[] arr = new Double[size];
        Arrays.fill(arr, 0D);
        return arr;
    }

    public static List<Dataset> clone(List<Dataset> list) {
        return list.stream().map(v -> v.clone()).collect(Collectors.toList());
    }

    public static void prettyPrint(List<Dataset> datasets) {
        datasets.stream().map(s -> s.prettyFormat()).collect(Collectors.toList()).forEach(System.out::println);
    }
}
