import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.util.AccumulatorV2;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ClusteringPso {

    public static void main(String[] args) {

        int PARTICLE_COUNT = 20;
        int iterMax = 500;
        final int FEATURE_SIZE = IrisData.FEATURE_SIZE;
        final int NUM_CLUSTERS = 3;
        final double DATASET_LOW = 0D;
        final double DATASET_HIGH = 6D;


        SparkConf conf = new SparkConf( ).setAppName( "Clustering PSO" );
//                            .setMaster("local[*]");  // run locally
        JavaSparkContext jsc = new JavaSparkContext( conf );

        // register accumulator for errors
//        ErrorAccumulator errorAccumulator = new ErrorAccumulator(PARTICLE_COUNT) ;
        FitnessAccumulator fitnessAccumulator = new FitnessAccumulator(PARTICLE_COUNT, NUM_CLUSTERS);
        jsc.sc().register(fitnessAccumulator);

        long startTime = System.currentTimeMillis();

        JavaRDD<String> input = jsc.textFile("iris_training.csv");
        JavaRDD<Dataset> dataset = input.map(line -> {
            String[] values = line.split(",");
            List<Double> features = new ArrayList<>();
            for (int i = 0; i < FEATURE_SIZE; ++i) {
                features.add(Double.parseDouble(values[i]));
            }
            int classId = Integer.parseInt(values[FEATURE_SIZE]);
            return new Dataset(features, classId);
        });

//        long dataSize = dataset.count();

        // Step 1: initialize Particles
        List<ClusterParticle> swarm = initializeParticles(FEATURE_SIZE, PARTICLE_COUNT, NUM_CLUSTERS, DATASET_LOW, DATASET_HIGH);

        Double minSwarmError = Double.MAX_VALUE;
        List<Dataset> globalBestPosition = swarm.get(0).centroids;


        // Step 2: iterations
        int iteration = 0;
        while (iteration < iterMax) {
            Random rand = new Random();
            Broadcast<List<ClusterParticle>> bcSwarm = jsc.broadcast(swarm);
            List<ClusterParticle> particles = bcSwarm.value();
            fitnessAccumulator.reset();
//            Double[] errors = errorAccumulator.value();

            dataset.foreachPartition(points -> {
                points.forEachRemaining(point -> {
                    List<ClusterDistance> bestDistances = particles.stream().map(particle -> {
                        Double bestDistance = Double.MAX_VALUE;
                        int bestClusterIndex = -1;
                        for (int i = 0; i < particle.centroids.size(); i++) {
                            Double distance = particle.centroids.get(i).euclideanDistance(point);
                            if (bestDistance > distance) {
                                bestDistance = distance;
                                bestClusterIndex = i;
                            }
                        }

                        return ClusterDistance.at(bestClusterIndex, bestDistance, NUM_CLUSTERS);
                    }).collect(Collectors.toList());
                    fitnessAccumulator.add(bestDistances.toArray(new ClusterDistance[bestDistances.size()]));
                });
            });

            ClusterDistance[] clusterDistances = fitnessAccumulator.value();

            for (int i = 0; i < particles.size(); i++) {
                ClusterParticle particle = particles.get(i);
                Double localBestValue = particle.getLocalBestValue();

                Double currentError = clusterDistances[i].fitness();

                if (currentError < localBestValue) {
                    if (currentError < minSwarmError) {
                        minSwarmError = currentError;
                        globalBestPosition = Util.clone(particle.centroids);
                    }
                    particle.setLocalBestValue(currentError);
                    particle.updateLocalBestPosition();
                }

                particle.updateVelocity(rand.nextDouble(), rand.nextDouble(), globalBestPosition);
                particle.updatePosition();
            }
            bcSwarm.destroy();

            iteration++;
        }
//        bcSwarm.destroy();

        Util.prettyPrint(globalBestPosition);
        System.out.println("Error: "+ minSwarmError);
        long endTime = System.currentTimeMillis();
        System.out.println("Duration: " + (endTime - startTime) + " ms.");
        jsc.stop();
    }

    private static List<ClusterParticle> initializeParticles(int dimensionCount, int particleCount, int numClusters, double boundaryLow, double boundaryHigh) {
        // Step 1: initialize Particles
        List<ClusterParticle> particles = new ArrayList<>();

        Random rand = new Random();
        for (int i = 0; i < particleCount; ++i) {
            // init centroid per cluster
            List<Dataset> centroids = new ArrayList<>();
            List<Dataset> velocity = new ArrayList<>();
            for (int j = 0; j < numClusters; ++j) {
                Dataset temp = new Dataset(dimensionCount, () -> rand.nextDouble()*(boundaryHigh-boundaryLow) + boundaryLow);
                Dataset tempVel = new Dataset(dimensionCount, () -> 0D);
                centroids.add(temp);
                velocity.add(tempVel);
            }
            ClusterParticle part = new ClusterParticle(centroids, velocity, Double.MAX_VALUE);
            particles.add(part);
        }
        return particles;
    }

    class IrisData {
        // 120,4,setosa,versicolor,virginica
        public Cluster cluster;
        public static final int TEST_SIZE = 30;
        public static final int TRAINING_SIZE = 120;
        public static final int FEATURE_SIZE = 4;
    }

    public enum Cluster {
        Setosa("setosa"), VersiColor("versicolor"), Virginica("virginica");

        private String name;

        Cluster(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }
    }

    static class ErrorAccumulator extends AccumulatorV2<Double[], Double[]> {

        private Double[] value;

        public ErrorAccumulator(int size) {

            this.value = Stream.generate(() -> 0D)
                .limit(size).collect(Collectors.toList())
                .toArray(new Double[size]);
        }

        private ErrorAccumulator(Double[] value) {
            this.value = value;
        }

        @Override
        public boolean isZero() {
            return stream().allMatch(v -> v == 0);
        }

        @Override
        public AccumulatorV2<Double[], Double[]> copy() {
            return new ErrorAccumulator(value);
        }

        @Override
        public void reset() {
            Arrays.fill(this.value, 0D);
        }

        @Override
        public void add(Double[] v) {
            for (int i = 0; i < v.length; i++) {
                this.value[i] += v[i];
            }
        }

        @Override
        public void merge(AccumulatorV2<Double[], Double[]> other) {
            add(other.value());
        }

        @Override
        public Double[] value() {
            return value;
        }

        private Stream<Double> stream() {
            return Arrays.asList(this.value).stream();
        }
    }

    static class FitnessAccumulator extends AccumulatorV2<ClusterDistance[], ClusterDistance[]> {

        private ClusterDistance[] value;

        public FitnessAccumulator(int size, int clusterCount) {
            this.value = Stream.generate(() -> ClusterDistance.zero(clusterCount))
                .limit(size).collect(Collectors.toList())
                .toArray(new ClusterDistance[size]);
        }

        private FitnessAccumulator(ClusterDistance[] value) {
            this.value = value;
        }

        @Override
        public boolean isZero() {
            return stream().allMatch(v -> v.isZero());
        }

        @Override
        public AccumulatorV2<ClusterDistance[], ClusterDistance[]> copy() {
            return new FitnessAccumulator(value);
        }

        @Override
        public void reset() {
            stream().forEach(v -> v.reset());
        }

        @Override
        public void add(ClusterDistance[] v) {
            for (int i = 0; i < v.length; i++) {
                this.value[i].add(v[i]);
            }
        }

        @Override
        public void merge(AccumulatorV2<ClusterDistance[], ClusterDistance[]> other) {
            add(other.value());
        }

        @Override
        public ClusterDistance[] value() {
            // calculate here
            return value;
        }

        private Stream<ClusterDistance> stream() {
            return Arrays.stream(this.value);
        }
    }

}
