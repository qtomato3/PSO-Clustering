
import java.io.Serializable;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class Dataset implements Serializable {
    Double[] record;
    private double dataLow = 0D;
    private double dataHigh = 1D;

    public Double[] getRecord() {
        return record.clone();
    }

    public void setRecord(List<Double> record) {
        this.record = (new ArrayList<>(record)).toArray(this.record);
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    Integer classId = -1;

//    public Dataset(int dimensionCount) {
//        this(dimensionCount, () -> 0D);
//    }
//
//    public Dataset(int dimensionCount, double low, double high, Random seeder) {
//        this(dimensionCount, () -> seeder.nextDouble()*(high-low) + low);
//    }

    public Dataset(int dimensionCount, Supplier<Double> valueSupplier) {
        this.record = new Double[dimensionCount];
        for (int i = 0; i < this.record.length; ++i) {
            this.record[i] = valueSupplier.get();
        }
    }

    public Dataset clone() {
        return new Dataset(Arrays.asList(record), classId);
    }

//    public Dataset() {
//        record = null;
//        classId = -1;
//    }
//
    public Dataset(List<Double> data, int classId) {
        Objects.requireNonNull(data);
        this.record = data.toArray(new Double[data.size()]);
        this.classId = classId;
    }


//    public Dataset(Double[] data, int classId) {
//        this.record = data.clone();
//        this.classId = classId;
//    }
//    public Dataset(Double[] data) {
//        this.record = data.clone();
//        this.classId = -1;
//    }

    // assume same dimension
    public Double euclideanDistance(Dataset another) {
        Double distance = 0D;
        Double[] anotherRecord = another.getRecord();
        if (this.record == null || anotherRecord == null || anotherRecord.length != this.record.length)
            return -1D;     // dimension mismatched or invalid data

        for (int i = 0; i < another.record.length; ++i) {
            distance += Math.pow(this.record[i] - anotherRecord[i], 2);
        }
        distance = Math.sqrt(distance);
        return distance;
    }

    public Double[] subtract(Double[] guest) {
        for (int i = 0; i < this.record.length; ++i) {
            this.record[i] -= guest[i];
        }
        return this.record;
    }
    public Double[] add(Double[] guest) {
        for (int i = 0; i < this.record.length; ++i) {
            this.record[i] += guest[i];
        }
        return this.record;
    }
    public Double[] multiply(Double s) {
        for (int i = 0; i < this.record.length; ++i) {
            this.record[i] *= s;
        }
        return this.record;
    }

    public String prettyFormat() {
        List<String> printableRecords = Arrays.asList(record).stream().map(v -> v.toString()).collect(Collectors.toList());
        return String.join(", ", printableRecords);
    }
}
