import java.io.Serializable;
import java.util.Arrays;

public class ClusterDistance implements Serializable {
    Distance[] distances;

    private ClusterDistance(Distance[] distances) {
        this.distances = distances;
    }

    public static ClusterDistance zero(int clusterCount) {
        Distance[] distances = new Distance[clusterCount];
        Arrays.fill(distances, new Distance());
        return new ClusterDistance(distances);
    }

    public static ClusterDistance at(int index, Double distance, int clusterCount) {
        ClusterDistance newInstance = zero(clusterCount);
        newInstance.distances[index] = new Distance(index, distance);
        return newInstance;
    }

    public static ClusterDistance copy(Distance[] distances) {
        return new ClusterDistance(distances);
    }

    public boolean isZero() {
        return Arrays.stream(distances).allMatch(v -> v.value < 0);
    }

    public void add(ClusterDistance clusterDistance) {
        for (int i = 0; i < distances.length; i++) {
            distances[i].add(clusterDistance.distances[i].value);
        }
    }

    public Double fitness() {
        Double sum = Arrays.stream(distances).mapToDouble(distance -> {
            double result = 0;
            if (distance.count > 0) {
                result = distance.value / distance.count;
            }
            return result;
        }).sum();
        return sum / distances.length;
    }

    public void reset() {
        Arrays.fill(distances, new Distance());
    }

    static class Distance implements Serializable {
        int count;
        Double value;

        public Distance() {
            this(0, -1D);
        }

        public Distance(int count, Double value) {
            this.count = count;
            this.value = value;
        }

        public void add(Double d) {
            if (d >= 0) {
                count++;
                value += d;
            }
        }
    }
}
