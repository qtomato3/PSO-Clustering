import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ClusterParticle implements Serializable {
    final List<Dataset> centroids;   // position
    final List<Dataset> velocity;

    Double localBestValue = Double.MAX_VALUE;
    List<Dataset> localBestPosition;



    public Double getLocalBestValue() {
        return localBestValue;
    }

    public void setLocalBestValue(Double localBestValue) {
        this.localBestValue = localBestValue;
    }

    public void updateLocalBestPosition() {
        this.localBestPosition = Util.clone(centroids);
    }

    public int getNumberOfClusters() {
        return centroids.size();
    }

//    public ClusterParticle() {
//        this(Collections.emptyList(), Collections.emptyList(), 0);
//    }

    public ClusterParticle(List<Dataset> init, List<Dataset> velocity, double local) {
        this.centroids = new ArrayList<>(init);
        this.velocity = new ArrayList<>(velocity);
        this.localBestValue = local;
        this.localBestPosition = new ArrayList<>(init);
    }

    public void updatePosition() {
        // X(t+1) = X(t) + V(t+1)
        for (int index = 0; index < this.centroids.size(); index++) {
            Dataset centroidPos = this.centroids.get(index);
            Dataset centroidVel = this.velocity.get(index);

            centroidPos.add(centroidVel.getRecord());
            // TODO: position boundaries

        }
    }
    private static final double DEFAULT_INERTIA = 0.729844;
    private static final double DEFAULT_COGNITIVE = 1.496180; // Cognitive component.
    private static final double DEFAULT_SOCIAL = 1.496180; // Social component.
    double inertia = DEFAULT_INERTIA;
    double cognitiveComponent = DEFAULT_COGNITIVE;
    double socialComponent = DEFAULT_SOCIAL;

    public void updateVelocity(Double r1, Double r2, List<Dataset> globalBest) {
        // V(t+1) = wV(t) + c1*r1(t)*(Xbest(t) - X(t)) + c2*r2(t)*(Xg_best(t) - X(t))
        for (int index = 0; index < this.velocity.size(); index++) {

            Dataset centroidPos = this.centroids.get(index);
            Dataset centroidVel = this.velocity.get(index);
            Dataset bestLocal = this.localBestPosition.get(index).clone();
            Dataset bestGlobal = globalBest.get(index).clone();

            bestGlobal.subtract(centroidPos.getRecord());
            bestLocal.subtract(centroidPos.getRecord());

            centroidVel.multiply(inertia);

            centroidVel.add(bestLocal.multiply(cognitiveComponent * r1));
            centroidVel.add(bestGlobal.multiply(socialComponent * r2));
            // TODO: velocity boundaries

        }
    }

}
